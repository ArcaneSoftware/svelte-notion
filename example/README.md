# svelte-notion-example

This is an example of a basic project using `svelte-notion`

Special thanks to the creators of `react-notion` and `notion-api-worker` for the API as well as the Notion page being loaded in the example.

# Installation

```
git clone git@gitlab.com:ArcaneSoftware/svelte-notion.git
cd ./svelte-notion/example/
npm i
npm run dev
```
