export const toNotionImageUrl = (url, blockId) =>
    `https://notion.so${
        url.startsWith('/image')
            ? url
            : `/image/${encodeURIComponent(url)}?table=block&id=${blockId}`
    }`

export const getTextContent = block =>
    block.properties?.title?.reduce((prev, current) => prev + current[0], '') ??
    ''
