import { writable } from 'svelte/store'

export const apiUrl = writable('https://notion-api.splitbee.io')
export const isFullPage = writable(false)
export const blocks = writable({})
